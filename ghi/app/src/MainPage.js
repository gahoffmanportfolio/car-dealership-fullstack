function MainPage() {
  //background image
  const myStyle = {
    backgroundImage: "url(/CarCarLogo.png)",
    height: "100vh",
    width: "100vh",
    margin:"auto",
    fontSize:'50px',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  }

  return (
    <div className="px-4 py-5 my-5 text-center">
      <div style={myStyle}>
        <h1 className="display-5 fw-bold">CarCar</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4"><b>
            The premiere solution for automobile dealership
            management!</b>
          </p>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
