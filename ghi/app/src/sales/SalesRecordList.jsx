import React, {useEffect, useState} from 'react';


function SalesRecordList () {
    const [load, setLoad] = useState(false);

    // Create a bunch of useState hooks and Change functions for the different inputs we need
    const [salesPersons, setSalesPersons] = useState([]);
    const [salesRecords, setSalesRecords] = useState([]);

    const [filter, setFilter] = useState('');
    const handleFilterChange = (event) => setFilter(event.target.value);


    // Load Data for list
    const getAll = async () => {
        // Get Sales Records
        const salesUrl = 'http://localhost:8090/api/sales_records/';

        const salesResponse = await fetch(salesUrl);

        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            setSalesRecords(salesData.sales_records)
        }
        // Get Sales Persons for Dropdown select
        const salesPersonUrl = 'http://localhost:8090/api/sales_persons/';
        const personResponse = await fetch(salesPersonUrl);
        if (personResponse.ok) {
            const salesPersonData = await personResponse.json();
            setSalesPersons(salesPersonData.sales_persons);
        }
        // Load state change for auto-updating
        if (true) {
            setLoad(!load);
        }
    };

    useEffect(() => {
        getAll();
    }, [load]);


    // return JSX
    return (
        <div className ="container">
            <h1>Sales Records</h1>
            <div className="mb-3">
                <select onChange={handleFilterChange} required name="sales_persons" id="sales_persons" value={filter} className="form-select">
                    <option value="">Choose a sales person</option>
                    {salesPersons.map(salesPerson => {
                        return (
                            <option key={salesPerson.id} value={salesPerson.name}>
                                {salesPerson.name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales person</th>
                        <th>Employee number</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale price</th>
                    </tr>
                </thead>
                <tbody>
                {salesRecords.filter((salesRecord) => {
                    return filter === '' ? salesRecord : salesRecord.sales_person.name.includes(filter);
                }).map(salesRecord => {
                    return (
                        <tr key={salesRecord.id}>
                            <td>{salesRecord.sales_person.name}</td>
                            <td>{salesRecord.sales_person.employee_number}</td>
                            <td>{salesRecord.customer.name}</td>
                            <td>{salesRecord.automobile.vin}</td>
                            <td><span>$</span>{salesRecord.price}</td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
    );
}

export default SalesRecordList;
