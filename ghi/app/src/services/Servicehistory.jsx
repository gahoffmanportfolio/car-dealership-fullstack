import React, { useEffect, useState} from 'react';



function ServiceHistory(){
    const[search, setSearch] = useState('')
    const [services, setServices] = useState([]);

    const handlesearchChange = (event) =>
    setSearch(event.target.value)

    const fetchData  = async () => {
      const listUrl = `http://localhost:8080/api/services/`;
      const response = await fetch(listUrl);

      if (response.ok) {
        const data = await response.json();
        setServices(data.service_appointments);
      }
    }

    useEffect(() => {
        fetchData();}, []);





    return (
        <div className="container">
                <h1 className=" text-center- mt-4">Service history</h1>
                <div className="form-outline mb-4">
                    <input onChange={handlesearchChange} type="search" className="form-control" id="datatable-search-input" placeholder='Enter Vin'/>
                </div>
                <div id="datatable">
                </div>
                <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Completed</th>
                    <th>VIN</th>
                    <th>Customer name</th>
                    <th>Date</th>
                    <th>time</th>
                    <th>Technician</th>
                    <th>reason</th>
                    </tr>
                </thead>
                <tbody>
                    {services.filter((service) =>{
                    return search === ''
                        ? service
                        : service.vin.includes(search)

                    }).map((service) => {
                        if(service.completed === true){
                            return service.completed = "Yes"
                        }
                    return (
                        <tr key={service.id} value={service.id}>
                        <td>{service.completed}</td>
                        <td>{ service.vin }</td>
                        <td>{ service.customer_name }</td>
                        <td>{ new Date(service.date).toLocaleDateString() }</td>
                        <td>{ new Date (service.time).toLocaleTimeString([], { hour: "2-digit"}) }</td>
                        <td>{ service.technician.name}</td>
                        <td>{ service.reason }</td>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
        </div>
    );
}
export default ServiceHistory
